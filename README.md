
[![pipeline status](https://gitlab.com/JayBigGuy10/jaybigguy10.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/JayBigGuy10/jaybigguy10.gitlab.io/-/commits/master)

---

JayGigGuy10s repository for hosting my personal blog, NorfolkNestBlog, with gitlab pages

Contact JBGuy at https://twitter.com/jaybigguy10 

---

## How the blog works

This project's static pages written in markdown which are built by the hugo static site generation tool according to a custom theme on JayBigGuy10s machine and then pushed to this repo which then begins a pipeline to deploy the site

```
<div class="units-row">

    <div class="unit-50">
        <ul class="social list-flat center">
          <li><a href="http://old.reddit.com/u/jaybigguy10"><i class="fa fa-reddit fa-2x"></i></a></li>
          <li><a href="http://twitter.com/jaybigguy10"><i class="fa fa-twitter fa-2x"></i></a></li>
          <li><a href="https://www.youtube.com/jaydenlitolff"><i class="fa fa-youtube fa-2x"></i></a></li>
        </ul>
    </div>
</div>
<p class="text-centered foot-cp">
    <a href="#">handcrafted by jayden litolff</a>
</p>
```

The above example is my footer